package lt.codeacademy.entities;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.Map;

public class Movie {

    private ObjectId id;

    private String title;

    private Integer year;

    private Double imdb;

    private String director;

    public Movie() {
    }

    public Movie(String title, Integer year, Double imdb, String director) {
        this.title = title;
        this.year = year;
        this.imdb = imdb;
        this.director = director;
    }

    public Movie(Document document) {
        this.id = document.getObjectId("_id");
        this.title = document.getString("title");
        this.year = document.getInteger("year");
        this.imdb = document.getDouble("imdb");
        this.director = document.getString("director");
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getImdb() {
        return imdb;
    }

    public void setImdb(Double imdb) {
        this.imdb = imdb;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Map<String, Object> toMap() {
        return new HashMap<>() {{
            put("title", title);
            put("year", year);
            put("imdb", imdb);
            put("director", director);
        }};
    }

    public Document toDocument() {
        return new Document(toMap());
    }
}
