package lt.codeacademy.services;

import lt.codeacademy.entities.Movie;
import lt.codeacademy.managers.DbConnector;
import org.bson.Document;

import java.util.*;

// CRUD service
public class MovieService {

    public void createMovie(Movie movie) {
        DbConnector.getCollection().insertOne(movie.toDocument());
    }

    public List<Movie> getMovies(Map<String, Object> map) {
        List<Movie> movies = new ArrayList<>();

        for (Document document : DbConnector.getCollection().find(new Document(map))) {
            movies.add(new Movie(document));
        }

        return movies;
    }

    public List<Movie> getAllMovies() {
        return getMovies(new HashMap<>());
    }

    public Movie getMovie(Map<String, Object> map) {
        Document document = DbConnector.getCollection().find(new Document(map)).first();
        Movie movie = null;

        if (document != null) {
            movie = new Movie();
            movie.setId(document.getObjectId("_id"));
            movie.setTitle(document.getString("title"));
            movie.setYear(document.getInteger("year"));
            movie.setImdb(document.getDouble("imdb"));
            movie.setDirector(document.getString("director"));
        }

        return movie;
    }

    public Movie getMovieByTitle(String title) {
        return getMovie(new HashMap<>() {{
            put("title", title);
        }});
    }

    public Movie getMovieByYear(Integer year) {
        return getMovie(new HashMap<>() {{
            put("year", year);
        }});
    }

    //TODO update method which takes movie as parameter if possible???
    public void updateOneMovieByTitle(String title, Movie movie) {
        updateOne(new HashMap<>() {{ put("title", title); }}, movie.toMap());
    }

    public void updateOne(Map<String, Object> filterMap, Map<String, Object> setMap) {
        DbConnector.getCollection().updateOne(new Document(filterMap), new Document("$set", new Document(setMap)));
    }

    public void updateMany(Map<String, Object> filterMap, Map<String, Object> setMap) {
        DbConnector.getCollection().updateMany(new Document(filterMap), new Document("$set", new Document(setMap)));
    }

    public void deleteOne(Movie movie) {
        DbConnector.getCollection().deleteOne(movie.toDocument());
    }
}
