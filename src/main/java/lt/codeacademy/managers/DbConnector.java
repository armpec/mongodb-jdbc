package lt.codeacademy.managers;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lt.codeacademy.utils.FileUtil;
import org.bson.Document;

import java.util.List;

public class DbConnector {

    private static final List<String> property = new FileUtil().readFromFile("database.properties");

    private static final MongoClient client = new MongoClient(new MongoClientURI(property.get(0).split("=")[1]));

    private static final MongoDatabase database = client.getDatabase(property.get(1).split("=")[1]);

    private static final MongoCollection<Document> collection = database.getCollection(property.get(2).split("=")[1]);

    public static MongoCollection<Document> getCollection() {
        return collection;
    }
}
