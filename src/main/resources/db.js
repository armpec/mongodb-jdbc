db.movies.drop();

db.movies.insertMany([
    {
        title: 'Interstellar',
        year: 2014,
        imdb: 8.6
    },
    {
        title: 'The Dark Knight',
        year: 2008,
        imdb: 9.1
    },
    {
        title: 'Inception',
        year: 2010,
        imdb: 8.8
    },
    {
        title: 'Tenet',
        year: 2020,
        imdb: 7.4
    }
]);